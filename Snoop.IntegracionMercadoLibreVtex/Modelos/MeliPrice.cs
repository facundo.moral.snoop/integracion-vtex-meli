﻿using System;
namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliPrice
    {
        public string id { get; set; }
        public string type { get; set; }
        public decimal amount { get; set; }      
        public decimal? regular_amount { get; set; }
        public string currency_id { get; set; }
        public string exchange_rate_context { get; set; }
    }
}
