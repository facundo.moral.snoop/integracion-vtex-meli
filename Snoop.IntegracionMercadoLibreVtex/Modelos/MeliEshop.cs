﻿using System;
namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliEshop
    {
        public string nick_name { get; set; }
        public string eshop_rubro { get; set; }
        public int eshop_id { get; set; }
        public string site_id { get; set; }
        public string eshop_logo_url { get; set; }
        public int eshop_status_id { get; set; }
        public long seller { get; set; }
        public int eshop_experience { get; set; }
    }
}
