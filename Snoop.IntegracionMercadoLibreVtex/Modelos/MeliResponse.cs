﻿using System;
namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliResponse
    {
        public MeliSeller seller { get;set; }
        public MeliPaging paging { get; set; }
    }
}
