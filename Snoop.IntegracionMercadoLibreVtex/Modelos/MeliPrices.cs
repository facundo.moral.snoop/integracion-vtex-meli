﻿using System;
using System.Collections.Generic;

namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliPrices
    {
        public List<MeliPrice> prices { get; set; }
        public string id { get; set; }
    }
}
