﻿using System;
namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliSeller
    {
        public int id { get; set; }
        public string nickname { get; set; }
        public string permalink { get; set; }
        public string registration_date { get; set; }
    }
}
