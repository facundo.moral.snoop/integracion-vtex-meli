﻿using System;
using System.Collections.Generic;

namespace Snoop.IntegracionMercadoLibreVtex.Modelos
{
    public class MeliProduct
    {
        public string id { get; set;}
        public string site_id { get; set; }
        public string title { get; set; }
        public MeliEshop eshop { get; set; }
        public long price { get; set; } 
        public MeliPrices prices { get; set; }
        //reference_prices
    }
}
